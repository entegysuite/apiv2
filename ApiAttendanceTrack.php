<?php

namespace EntegyPlugin\ApiV2;


class ApiAttendanceTrack extends ApiV2
{
     public function __construct($config = [])
     {
         parent::__construct($config);
     }

     public function addCheckIn($contentExternalReference = '', $contentModuleId = '', $profileId = '', $profileSecondaryId = '', $profileExternalReference = '', $profileInternalReference = '', $profileBadgeReference = '')
     {
         $input = [
             'profileReference',
             'contentReference'
         ];

         if (!empty($contentExternalReference)) $input ['contenReference']['externalReference'] = $contentExternalReference;
         else if (!empty($contentModuleId)) $input ['contentReference']['moduleId'] = $contentModuleId;
         else return [
             'response' => 401,
             'message' => "Missing content reference"
         ];

         if (!empty($profileId)) $input ['profileReference']['profileId'] = $profileId;
         else if (!empty($profielExternalReferece)) $input ['profileReference']['externalReference'] = $profileExternalReference;
         else if (!empty($profileInternalReference)) $input ['profileReference']['internalReference'] = $profileInternalReference;
         else if (!empty($profileBadgeReference)) $input ['profileReference']['badgeReference'] = $profileBadgeReference;
         else if (!empty($profileSecondaryId)) $input ['profileReference']['secondaryId'] = $profileSecondaryId;
         else return [
             'response' => 402,
             'message' => "Missing profile reference"
         ];

         $response = $this->getJsonPost($input, '/v2/Track/AddCheckin');
         return $response;
     }

     public function getSessionAttendees($moduleId = '', $externalReference = '', $cacheTime = 0)
     {
         $input = [];
         if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
         else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
         else return [
             'response' => 401,
             'message' => "Missing content reference"
         ];

         $response = $this->getJsonPost($input, "/v2/Track/Attendees", $cacheTime);
         return $response;
     }

     public function getAttendeeSessions($profileId = '', $secondaryId = '', $externalReference = '', $badgeReference = '', $internalReference = '', $cacheTime = 0)
     {
         $input = [];
         if (!empty($profileId)) $input ['profileId'] = $profileId;
         else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
         else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
         else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
         else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
         else return [
             'response' => 401,
             'message' => 'Missing profile reference'
         ];

         $response = $this->getJsonPost($input, "/v2/Track/Attended", $cacheTime);
         return $response;
     }

}