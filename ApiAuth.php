<?php

namespace EntegyPlugin\ApiV2;

class ApiAuth extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function loginToProject($profileId, $deviceId)
    {
        $input = [
            "profileId" => $profileId,
            "deviceId" => $deviceId
        ];

        $response = $this->getJsonPost($input, "/v2/Authentication/ExternalProfile");
        return $response;
    }
}
