<?php

namespace EntegyPlugin\ApiV2;

class ApiCategories extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function selectCategories($templateType, $categories, $moduleId, $externalReference = "")
    {
        return $this->doCategoryCall("/v2/Categories/Select", $templateType, $categories, $moduleId, $externalReference);
    }

    public function deselectCategories($templateType, $categories, $moduleId, $externalReference = "")
    {
        return $this->doCategoryCall("/v2/Categories/Deselect", $templateType, $categories, $moduleId, $externalReference);
    }

    // [ "name" => "My Category", "externalReference" => "myextref" ]
    public function createCategory($templateType, $categories, $moduleId = "", $externalReference = "")
    {
        return $this->doCategoryCall("/v2/Categories/Create", $templateType, $categories, $moduleId, $externalReference);
    }

    public function createCategoryChild($categories, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (isset($moduleId)) $input ['moduleId'] = $moduleId;
        else if (isset($externalReference)) $input ['externalReference'] = $externalReference;

        if (isset($categories)) $input ['categories'] = $categories;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $response = $this->getJsonPost($input, '/v2/Categories/CreateChild');
        return $response;
    }

    public function availableCategories($templateType, $moduleId, $externalReference = "")
    {
        $input = [
            "templateType" => $templateType,
            "externalReference" => $externalReference
        ];

        if($moduleId) $input["moduleId"] = $moduleId;

        $response = $this->getJsonPost($input, "/v2/Categories/Available");

        if($response["response"] == 200) return true;
        return false;
    }

    public function updateCategory($name, $moduleId, $externalReference = "")
    {
        $input = [];
        if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else return [
            'response' => 401,
            'message' => "Missing Id"
        ];

        if (!empty($name)) $input ['name'] = $name;
        else return [
            'response' => 402,
            'message' => "Missing Name"
        ];

        $response = $this->getJsonPost($input, "/v2/Categories/Update");

        return $response;
    }

    public function deleteCategory($templateType, $categories, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $input ['templateType'] = $templateType;
        $input ['categories'] = $categories;

        $response = $this->getJsonPost($input, '/v2/Categories/Delete');
        return $response;
    }

    // $categories is multiarray of categories you wish to perform actions with
    // eg: [ "name" => "mycategory" ], [ "externalReference" => "category_extref" ], [ "moduleId" => "category_modId" ]
    public function doCategoryCall($uri, $templateType, $categories, $moduleId, $externalReference = "")
    {
        $input = [
            "templateType" => $templateType,
            "externalReference" => $externalReference,
            "categories" => $categories
        ];

        if($moduleId) $input["moduleId"] = $moduleId;

        $response = $this->getJsonPost($input, $uri);

        return $response;
    }
}
