<?php

namespace EntegyPlugin\ApiV2;

use Firebase\JWT\JWT;

class ApiContent extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getContent($template, $module, $externalRef = "", $includeCategories = false, $includeDocuments = false, $includeLinks = false, $includeMultiLinks = false, $cacheTime = 0, $cacheName = "", $includePageSettings = false)
    {
        if (!empty($externalRef)) $input ['externalReference'] = $externalRef;
        else $input ['moduleId'] = $module;

        $input += [
            "templateType" => $template,
            "includeCategories" => $includeCategories,
            "includeLinks" => $includeLinks,
            "includeMultiLinks" => $includeMultiLinks,
            "includeDocuments" => $includeDocuments,
            "includePageSettings" => $includePageSettings
        ];

        $response = $this->getJsonPost($input, "/v2/Content", $cacheTime, $cacheName);
        return $response;
    }

    public function getSchedule($template, $module, $externalRef = "", $includeCategories = false, $includeDocuments = false, $includeLinks = false, $includeMultiLinks = false, $cacheTime = 0, $cacheName = "")
    {
        if (!empty($externalRef)) $input ['externalReference'] = $externalRef;
        else $input ['moduleId'] = $module;

        $input += [
            "templateType" => $template,
            "includeCategories" => $includeCategories,
            "includeLinks" => $includeLinks,
            "includeMultiLinks" => $includeMultiLinks,
            "includeDocuments" => $includeDocuments
        ];

        $response = $this->getJsonPost($input, "/v2/Content/Schedule", $cacheTime, $cacheName);
        return $response;
    }

    public function createContent($templateType, $name, $externalRef = "", $contentGroup = "Default", $otherFields = [])
    {
        $input = [
            "contentGroup" => $contentGroup,
            "content" => [
                "templateType" => $templateType,
                "externalReference" => $externalRef,
                "name" => $name
            ]
        ];

        $input["content"] = array_merge($input["content"], $otherFields);

        $response = $this->getJsonPost($input, "/v2/Content/Create");
        return $response;
    }

    public function addContentChildren($parentTemplate, $childTemplateType, $children, $parentModuleId = '', $parentExternalRef = '')
    {
        // ----------------
        // Children is array of arrays with the fields:
        // name (required)
        // externalReference
        // strings [] content for the child
        // category category ID of the child
        // links [] links to other template types / modules
        if (!empty($parentModuleId)) $input ['moduleId'] = $parentModuleId;
        else if (!empty($parentExternalRef)) $input ['externalReference'] = $parentExternalRef;

        $input += [
            "templateType" => $parentTemplate,
            "childTemplateType" => $childTemplateType,
            "children" => $children
        ];

        $response = $this->getJsonPost($input, "/v2/Content/AddChildren");
        return $response;
    }

    public function updateContent($template, $module, $externalReference = "", $updateFields = [])
    {
        $input = [
            "templateType" => $template,
            "externalReference" => $externalReference,
            "content" => $updateFields
        ];

        if(!$externalReference) $input["moduleId"] = $module;

        $response = $this->getJsonPost($input, "/v2/Content/Update");
        return $response;
    }

    /**
     * @param string $templateType Template type of the content
     * @param string $externalRef External Ref of the content
     * @param null $moduleId
     * @return bool
     */
    public function checkContentExists($templateType, $externalRef = "", $moduleId = null)
    {
        $input = [
            "templateType" => $templateType,
            "externalReference" => $externalRef
        ];

        if($moduleId) $input["moduleId"] = $moduleId;

        $response = $this->getJsonPost($input, "/v2/Content/Exists");

        return $response;
    }

    public function deleteContent($templateType, $externalRef = "", $moduleId = null)
    {
        $input = [
            "templateType" => $templateType,
//            "externalReference" => $externalRef
        ];

        if($moduleId) $input["moduleId"] = $moduleId;

        $response = $this->getJsonPost($input, "/v2/Content/Delete", 0, '', "DELETE");

        return $response;
    }
}
