<?php

namespace EntegyPlugin\ApiV2;

class ApiDocuments extends ApiV2
{
    public function __construct($config)
    {
        parent::__construct($config);
    }

    public function addFile($fileDocuments, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => "Missing Id"
        ];

        $input ['templateType'] = $templateType;
        $input ['fileDocuments'] = $fileDocuments;

        $response = $this->getJsonPost($input, '/v2/Document/AddFile');
        return $response;
    }

    public function addExternalLink($externalLinkDocuments, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $input ['templateType'] = $templateType;
        $input ['externalLinkDocuments'] = $externalLinkDocuments;

        $response = $this->getJsonPost($input, '/v2/Document/AddExternalLink');
        return $response;
    }

    public function addText($textDocuments, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $input ['templateType'] = $templateType;
        $input ['textDocuments'] = $textDocuments;

        $response = $this->getJsonPost($input, '/v2/Documents/AddText');
        return $response;
    }

    public function addHTML($htmlDocuments, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $input ['templateType'] = $templateType;
        $input ['htmlDocuments'] = $htmlDocuments;

        $response = $this->getJsonPost($input, '/v2/Documents/AddHtml');
        return $response;
    }

    public function addExternalContent($externalContent, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing Id'
        ];

        $input ['templateType'] = $templateType;
        $input ['externalContent'] = $externalContent;

        $response = $this->getJsonPost($input, '/v2/Documents/ExternalContent');
        return $response;
    }
}
