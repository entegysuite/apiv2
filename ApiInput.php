<?php

namespace EntegyPlugin\ApiV2;

class ApiInput
{
    public $projectId;
    public $apiKey;
    public $requestNonce;

    public function __construct($projectId, $apiKey, $requestNonce)
    {
        $this->projectId = $projectId;
        $this->apiKey = $apiKey;
        $this->requestNonce = $requestNonce;
    }

    public function get()
    {
        return get_object_vars($this);
    }
}
