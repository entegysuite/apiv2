<?php

namespace EntegyPlugin\ApiV2;


class ApiLeadCapture extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    

    public function addLead ($contentModuleId = '', $contentExternalReference = '', $profileId = '', $profileSecondaryReference = '', $profileExternalReference = '', $profileInternalReference = '', $profileBadgeReference = '')
    {
        $input = [
            'profileReference' => [],
            'contentReference' => []
        ];

        if (!empty ($profileId)) $input['profileReference']['profileId'] = $profileId;
        else if (!empty($profileExternalReference)) $input['profileReference']['externalReference'] = $profileExternalReference;
        else if (!empty($profileInternalReference)) $input['profileReference']['internalReference'] = $profileInternalReference;
        else if (!empty($profileBadgeReference)) $input['profileReference']['badgeReference'] = $profileBadgeReference;
        else if (!empty($profileSecondaryReference)) $input['profileReference']['secondaryReference'] = $profileSecondaryReference;
        else return [
            'response' => 402,
            'message' => "Missing profile reference"
        ];

        if (!empty ($contentExternalReference)) $input['contentReference']['externalReference'] = $contentExternalReference;
        else if(!empty ($contentModuleId)) $input['contentReference']['moduleId'] = $contentModuleId;
        else return [
            'response' => 401,
            'message' => "Missing content reference"
        ];

         $response = $this->getJsonPost($input, '/v2/Lead/Add');
         return $response;
    }

    public function getExhibitorLeads ($moduleId = '', $externalReference = '', $cacheTime = 0)
    {
        $input = [];

        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => "Missing content reference"
        ];

        $response = $this->getJsonPost($input, '/v2/Lead/Exhibitor', $cacheTime);
        return $response;
    }

    public function getProfileLeads ($profileId = '', $secondaryReference = '', $externalReference = '', $internalReference = '', $badgeReference = '', $cacheTime = '')
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($secondaryReference)) $input ['secondaryReference'] = $secondaryReference;
        else return [
            'response' => 401,
            'message' => 'Missing profile reference'
        ];

        $response = $this->getJsonPost($input, '/v2/Lead/Profile', $cacheTime);
        return $response;
    }
}