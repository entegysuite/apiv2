<?php

namespace EntegyPlugin\ApiV2;

class ApiMultiLink extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getMultiLink($templateType, $externalReference = "", $moduleId = "")
    {
        $input = [];

        if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else return [
            'response' => 401,
            'message' => "Missing ID"
        ];

        $input ['templateType'] = $templateType;

        $response = $this->getJsonPost($input, "/v2/MultiLink/");
        return $response;
    }

    public function addMultiLink($multiLinks, $templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];

        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing ID'
        ];

        $input ['templateType'] = $templateType;
        $input ['multiLinks'] = $multiLinks;

        $response = $this->getJsonPost($input, "/v2/MultiLink/Add");
        return $response;
    }

    public function removeMultiLink($templateType, $targetTemplateType, $moduleId = '', $externalReference = '', $targetModuleId = '', $targetExternalReference = '')
    {
        $input = [];

        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => 'Missing ID'
        ];

        if (!empty($targetModuleId)) $input ['targetModuleId'] = $targetModuleId;
        else if (!empty($targetExternalReference)) $input ['targetExternalReference'] = $targetExternalReference;
        else return [
            'response' => 401,
            'message' => 'Missing ID'
        ];

        $input ['templateType'] = $templateType;
        $input ['targetTemplateType'] = $targetTemplateType;

        $response = $this->getJsonPost($input, "/v2/MultiLink/Remove");
        return $response;
    }

    public function removeAllMultiLinks($templateType, $linkTemplateType, $moduleId = '', $externalReference = '')
    {
        $input = [];

        if (!empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else return [
            'response' => 401,
            'message' => "Missing ID"
        ];

        $input ['templateType'] = $templateType;
        $input ['linkTemplateType'] = $linkTemplateType;

        $response = $this->getJsonPost($input, "/v2/MultiLink/RemoveAll");
        return $response;
    }

}
