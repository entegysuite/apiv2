<?php

namespace EntegyPlugin\ApiV2;

class ApiNotification extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function sendNotification($recipient, $title, $message, $alertMessage, $targetPage = array(), $args = array())
    {
        if (!is_array($recipient) || empty($recipient) || $title == "" || $message == "")
        {
            return false;
        }

        $input = [];

        if (array_key_exists('profileId', $recipient))
        {
            $input["profileId"] = $recipient['profileId'];
        } else if (array_key_exists('profileExternalReference', $recipient))
        {
            $input["profileExternalReference"] = $recipient['profileExternalReference'];
        }

        $input['title'] = $title;
        $input['message'] = $message;
        $input['alertMessage'] = $alertMessage;

        if (!empty($targetPage))
        {
            $input['viewTargetPage'] = $targetPage;
        }

        if (!empty($args))
        {
            $input['arguments'] = $args;
        }

        $response = $this->getJsonPost($input, "/v2/Notification/SendDirect");
        return $response;
    }

}
