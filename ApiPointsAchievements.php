<?php

namespace EntegyPlugin\ApiV2;

class ApiPointsAchievements extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getAllAchievements($cacheTime = 300)
    {
        $response = $this->getJsonPost([], '/v2/Achievement/All', $cacheTime);
        return $response;
    }

    public function awardAchievement($achievementId, $sendNotifications = false, $profileId = '', $secondaryId = '', $externalReference = '', $badgeReference = '', $internalReference = '')
    {
        $input = [
            'achievementId' => $achievementId,
            'sendNotifications' => $sendNotifications
        ];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else {
            return [
                'response' => 402,
                'message' => 'Missing profile reference'
            ];
        }

        $response = $this->getJsonPost($input, '/v2/Achievement/Award');
        return $response;
    }

    public function getMultiAchievementObject($achievementId, $profileIds = [], $secondaryIds = [], $externalReferences = [], $internalReferences = [], $badgeReferences = []) {
        // First make profile references:
        $output = ['achievementId' => $achievementId];
        $profileReferences = [];

        foreach ($profileIds as $id) {
            $profileReferences [] = ['profileId' => $id];
        }
        foreach ($secondaryIds as $id) {
            $profileReferences [] = ['secondaryId' => $id];
        }
        foreach ($externalReferences as $id) {
            $profileReferences [] = ['externalReference' => $id];
        }
        foreach ($internalReferences as $id) {
            $profileReferences [] = ['internalReference' => $id];
        }
        foreach ($badgeReferences as $id) {
            $profileReferences [] = ['badgeReference' => $id];
        }

        $output ['profileReferences'] = $profileReferences;
        return $output;
    }

    public function awardMultipleAchievements($multiAchievementObjects, $sendNotifications = false)
    {
        $input = [
            'sendNotifications' => $sendNotifications,
            'achievements' => $multiAchievementObjects
        ];
        $response = $this->getJsonPost($input, '/v2/Achievement/MultiAward');
        return $response;
    }

    public function revokeAchievement($achievementId, $profileId = '', $secondaryId = '', $externalReference = '', $badgeReference = '', $internalReference = '')
    {
        $input = ['achievementId' => $achievementId];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else {
            return [
                'response' => 402,
                'message' => 'Missing profile reference'
            ];
        }

        $response = $this->getJsonPost($input, '/v2/Achievement/Revoke');
        return $response;
    }

    public function revokeMultipleAchievements($multiAchievementObjects)
    {
        $response = $this->getJsonPost(['achievements' => $multiAchievementObjects], '/v2/Achievement/MultiRevoke');
        return $response;
    }

    public function getEarnedAchievements($profileId = '', $secondaryId = '', $externalReference = '', $internalReference = '', $badgeReference = '', $cacheTime = 0)
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => 'Missing profile reference'
        ];

        $response = $this->getJsonPost($input, '/v2/Achievement/Earned', $cacheTime);
        return $response;
    }

    public function getProfilesWithAchievement($achievementId, $cacheTime = 0)
    {
        $input = ['achievementId' => $achievementId];
        $response = $this->getJsonPost($input, '/v2/Achievement/GivenTo', $cacheTime);
        return $response;
    }

    public function awardPoints($pointAmount, $pointEvent, $profileId = '', $secondaryId = '', $externalReference = '', $internalReference = '', $badgeReference = '')
    {
        $input = [
            'points' => $pointAmount,
            'pointEvent' => $pointEvent,
        ];

        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => "Missing profile reference"
        ];

        $response = $this->getJsonPost($input, '/v2/Point/Award');
        return $response;
    }

    public function getEarnedPoints($profileId = '', $secondaryId = '', $externalReference = '', $internalReference = '', $badgeReference = '', $cacheTime = 0)
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => "Missing profile reference"
        ];

        $response = $this->getJsonPost($input, '/v2/Point/Earned', $cacheTime);
        return $response;
    }

    public function getLeaderboard($cacheTime = 0)
    {
        $response = $this->getJsonPost([], '/v2/Point/Leaderboard', $cacheTime);
        return $response;
    }
}