<?php

namespace EntegyPlugin\ApiV2;


class ApiProfileCustomFields extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getCustomField ($key)
    {
        $input = [
            'key' => $key
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileCustomField/');
        return $response;
    }

    public function createCustomField ($customField)
    {
        $input = [
            'customField' => $customField
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileCustomField/Create/');
        return $response;
    }

    public function updateCustomField ($key, $customField)
    {
        $input = [
            'key' => $key,
            'customField' => $customField
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileCustomField/Update/');
        return $response;
    }

    public function deleteCustomField ($key)
    {
        $input = [
            'key' => $key
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileCustomField/Delete/');
        return $response;
    }

    public function getAllCustomFields ($pagination = ['start' => 0, 'limit' => 1000], $cacheTime = 300)
    {
        //$response = $this->getPagedRequest([], "/v2/ProfileCustomField/All/", 'customFields', $pagination, $cacheTime, "allCustomFields");
        $response = $this->getJsonPost([], "/v2/ProfileCustomField/All/", $cacheTime, 'customFields');
        return $response;
    }
}
