<?php

namespace EntegyPlugin\ApiV2;

class ApiProfileLinks extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function selectProfileLink ($link, $profileId = '', $externalRef = '', $internalRef = '', $badgeRef = '', $secondaryId = '')
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalRef)) $input ['externalReference'] = $externalRef;
        else if (!empty($internalRef)) $input ['internalReference'] = $internalRef;
        else if (!empty($badgeRef)) $input ['badgeReference'] = $badgeRef;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => 'Profile not found'
        ];

        $input ['link'] = $link;

        $response = $this->getJsonPost($input, '/v2/ProfileLink/Select/');
        return $response;
    }

    public function selectMultipleProfileLinks ($profiles)
    {
        $input = [];
        if (!empty($profiles)) $input ['profiles'] = $profiles;
        else return [
            'response' => 401,
            'message' => "Profile doesn't exist"
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileLink/MultiSelect/');
        return $response;
    }

    public function deselectProfileLink ($link, $profileId = '', $externalRef = '', $internalRef = '', $badgeRef = '', $secondaryId = '')
    {
        $input = [];
        if (!empty ($profileId)) $input ['profileId'] = $profileId;
        else if (!empty ($externalRef)) $input ['externalReference'] = $externalRef;
        else if (!empty ($internalRef)) $input ['internalReference'] = $internalRef;
        else if (!empty ($badgeRef)) $input ['badgeReference'] = $badgeRef;
        else if (!empty ($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return[
            'response' => 401,
            'message' => 'Profile not found'
        ];

        $input ['link'] = $link;

        $response = $this->getJsonPost($input, '/v2/ProfileLink/Deselect');
        return $response;
    }

    public function selectedProfileLink ($profileId = '', $externalRef = '', $internalRef = '', $badgeRef = '', $secondaryId = '')
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalRef)) $input ['externalReference'] = $externalRef;
        else if (!empty($internalRef)) $input ['internalReference'] = $internalRef;
        else if (!empty($badgeRef)) $input ['badgeReference'] = $badgeRef;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => 'Profile not found'
        ];

        $response = $this->getPagedRequest($input, '/v2/ProfileLink/Selected/', 'profileLinks');
        return $response;
    }

    public function clearProfileLink ($templateType, $profileId = '', $externalRef = '', $internalRef = '', $badgeRef = '', $secondaryId = '')
    {
        $input = [];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($externalRef)) $input ['externalReference'] = $externalRef;
        else if (!empty($internalRef)) $input ['internalReference'] = $internalRef;
        else if (!empty($badgeRef)) $input ['badgeReference'] = $badgeRef;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else return [
            'response' => 401,
            'message' => 'Profile not found'
        ];

        $input ['templateType'] = $templateType;

        $response = $this->getJsonPost($input, '/v2/ProfileLink/Clear');
        return $response;
    }

    public function pageProfileLink($templateType, $moduleId = '', $externalReference = '')
    {
        $input = [];
        if (empty($moduleId)) $input ['moduleId'] = $moduleId;
        else if (!empty ($externalReference)) $input ['externalReference'] = $externalReference;

        if (!empty($templateType)) $input ['templateType'] = $templateType;
        else return [
            'response' => 402,
            'message' => 'Linked content not found'
        ];

        $response = $this->getPagedRequest($input, '/v2/ProfileLink/Page/', 'profiles');
        return $response;
    }
}
