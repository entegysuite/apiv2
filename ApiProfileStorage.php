<?php

namespace EntegyPlugin\ApiV2;

class ApiProfileStorage extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getStorageObject($storageKey, $storageType, $data, $clientUpdated = '')
    {
        $output = [
            'storageKey' => $storageKey,
            'storageType' => $storageType,
            'data' => $data
        ];
        if (!empty($clientUpdated)) $output ['clientUpdated'] = $clientUpdated;
        else {
                $output ['clientUpdated'] = date('Y-m-d H:i:s');
        }
        return $output;
    }

    public function getProfileStorage($storageType, $storageKey = '', $profileId = '', $secondaryId = '', $externalReference = '', $internalReference = '', $badgeReference = '', $clientUpdatedAfter = '', $serverUpdatedAfter = '', $cacheTime = 0)
    {
        $input = ['storageType' => $storageType];
        if (!empty($storageKey)) $input ['storageKey'] = $storageKey;
        if (!empty($clientUpdatedAfter)) $input ['clientUpdatedAfter'] = $clientUpdatedAfter;
        if (!empty($serverUpdatedAfter)) $input ['serverUpdatedAfter'] = $serverUpdatedAfter;
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else return [
            'response' => 401,
            'message' => 'Missing profile reference'
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileStorage', $cacheTime);
        return $response;
    }

    public function updateProfileStorage($storageObjects = [], $profileId = '', $secondaryId = '', $externalReference = '', $internalReference = '', $badgeReference = '')
    {
        $input = ['storage' => $storageObjects];
        if (!empty($profileId)) $input ['profileId'] = $profileId;
        else if (!empty($secondaryId)) $input ['secondaryId'] = $secondaryId;
        else if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        else if (!empty($internalReference)) $input ['internalReference'] = $internalReference;
        else if (!empty($badgeReference)) $input ['badgeReference'] = $badgeReference;
        else return [
            'response' => 401,
            'message' => "Missing profile reference"
        ];

        $response = $this->getJsonPost($input, '/v2/ProfileStorage/Update');
        return $response;
    }
}