<?php

namespace EntegyPlugin\ApiV2;

class ApiProfileType extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getProfileType($name = '', $externalReference = '')
    {
        $input = [];

        if (!empty($name)) $input ['name'] = $name;
        if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        if (empty ($name) && empty ($externalReference)) return [
            'response' => 401,
            'message' => "Profile type not found"
        ];

        $response = $this->getJsonPost($input, "/v2/ProfileType/");
        return $response;
    }

    public function createProfileType($profileType)
    {
        $input = [];

        if (!empty ($profileType)) $input ['profileType'] = $profileType;
        else return [
            'response' => 402,
            'message' => "Missing Data"
        ];

        $response = $this->getJsonPost($input, "/v2/ProfileType/Create");
        return $response;
    }

    public function updateProfileType($profileType, $name= '', $externalReference = '')
    {
        $input = [];

        if (!empty($name)) $input ['name'] = $name;
        if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        if (empty($name) && empty($externalReference)) return [
            'response' => 401,
            'message' => 'Profile Type not found'
        ];

        $input ['profileType'] = $profileType;

        $response = $this->getJsonPost($input, "/v2/ProfileType/Update");
        return $response;
    }

    public function deleteProfileType($name = '', $externalReference = '')
    {
        $input = [];

        if (!empty($name)) $input ['name'] = $name;
        if (!empty($externalReference)) $input ['externalReference'] = $externalReference;
        if (empty ($name) && empty($externalReference)) return [
            'response' => 401,
            'message' => 'Profile Type not found'
        ];

        $response = $this->getJsonPost($input, "/v2/ProfileType/Delete", 0, '', 'DELETE');
        return $response;
    }

    public function getAllProfileTypes($pagination = ["start" => 0, "limit" => 1000], $cacheTime = 300)
    {
        //$response = $this->getPagedRequest([], "/v2/ProfileType/All/", 'profileTypes', $pagination, $cacheTime);
        $response = $this->getJsonPost([], "/v2/ProfileType/All/", $cacheTime, 'profileTypes');
        return $response;
    }
}
