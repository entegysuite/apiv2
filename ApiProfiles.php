<?php

namespace EntegyPlugin\ApiV2;

class ApiProfiles extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function getProfile($profileId, $externalRef = "", $internalRef = "", $badgeRef = "", $secondaryId = "", $cacheTime = 0, $includePermissions = false, $includeCustomFields = false)
    {
        $input = [];

        if(!empty($externalRef)) $input ["externalReference"] = $externalRef;
        else if (!empty($internalRef)) $input ["internalReference"] = $internalRef;
        else if (!empty($badgeRef)) $input ["badgeReference"] = $badgeRef;
        else if (!empty($profileId)) $input ["profileId"] = $profileId;
        else if (!empty($secondaryId)) $input ["secondaryId"] = $secondaryId;
        else return [
            "response" => 400,
            "message" => "Profile doesn't exist"
        ];

        $input['includePermissions'] = $includePermissions;
        $input['includeCustomFields'] = $includeCustomFields;

        $response = $this->getJsonPost($input, "/v2/Profile", $cacheTime);
        return $response;
    }

    public function createProfile($profile)
    {
        if (!empty($profile)) {
            $input ['profile'] = $profile;

            if (isset($input ['profile']['customFields']) && empty($input ['profile']['customFields'])) {
                unset($input ['profile']['customFields']);
            }

            $response = $this->getJsonPost($input, "/v2/Profile/Create");
            return $response;
        }
        else return [
            "response" => 400,
            "message" => "Invalid custom field key or value"
        ];
    }

    public function updateProfile($profileId, $profile, $externalRef = "", $internalRef = "", $badgeRef = "", $secondaryId = "")
    {
       $input = [];

       if (!empty($externalRef)) $input ["externalReference"] = $externalRef;
       else if (!empty($internalReference)) $input ["internalReference"] = $internalRef;
       else if (!empty($badgeRef)) $input ["badgeReference"] = $badgeRef;
       else if (!empty($profileId)) $input ["profileId"] = $profileId;
       else if (!empty($secondaryId)) $input ["secondaryId"] = $secondaryId;
       else return [
           "response" => 401,
           "message" => "No profile found"
       ];

       if (!empty($profile)) $input ["profile"] = $profile;
       else return [
           "response" => 402,
           "message" => "Nothing given to change"
       ];

        if (isset($input ['profile']['customFields']) && empty($input ['profile']['customFields'])) {
            unset($input ['profile']['customFields']);
        }

       $response = $this->getJsonPost($input, "/v2/Profile/Update/");
       return $response;
    }

    public function deleteProfile($profileId, $externalRef = "", $internalRef = "", $badgeRef = "", $secondaryId = "", $cacheTime = 0)
    {
        $input = [];

        if (!empty($externalRef)) $input ["externalReference"] = $externalRef;
        else if (!empty($internalRef)) $input ["internalReference"] = $internalRef;
        else if (!empty($badgeRef)) $input ["badgeReference"] = $badgeRef;
        else if (!empty($profileId)) $input ["profileId"] = $profileId;
        else if (!empty($secondaryId)) $input ["secondaryId"] = $secondaryId;
        else return [
            "response" => 401,
            "message" => "No profile found"
        ];

        $response = $this->getJsonPost($input, "/v2/Profile/Delete", $cacheTime, '', "DELETE");
        return $response;
    }

    public function getAllProfiles($pagination = ["start" => 0, "limit" => 1000], $filterId = "", $cacheTime = 300, $includePermissions = false, $includeCustomFields = false)
    {
        $input = [];

        if (!empty($filterId)) $input ['filterId'] = $filterId;

        $input['includePermissions'] = $includePermissions;
        $input['includeCustomFields'] = $includeCustomFields;
        $input['status'] = "all";

        $response = $this->getPagedRequest($input, '/v2/Profile/All', 'profiles', $pagination, $cacheTime, 'allProfiles');
        return $response;
    }

    // When Creating Profiles, firstName, lastName and type are required
    public function syncProfiles($updateReferenceType, $profiles)
    {
        $input = [];

        $input ['updateReferenceType'] = $updateReferenceType;
        $input ['profiles'] = $profiles;
        foreach ($input ['profiles'] as $profile) {
            if (isset($profile['customFields']) && empty($profile['customFields'])) {
                unset($profile['customFields']);
            }
        }

        $response = $this->getJsonPost($input, '/v2/Profile/Sync/');
        return $response;

    }
}
