<?php

namespace EntegyPlugin\ApiV2;


class ApiProject extends ApiV2
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    public function createDemo($distributorKey, $projectInfo, $creatorInfo) {

        if (empty($projectInfo['regionId']) || empty($projectInfo['templateProjectId']) || empty($projectInfo['appPortalBuildId']) ||
            empty($projectInfo['capturePortalBuildId']) || empty($projectInfo['eventCode']) || empty($projectInfo['projectName']) ||
            empty($projectInfo['projectShortName']))
        {
            return [
                'response' => '401',
                'message' => 'Missing project details'
            ];
        }

        if (empty($creatorInfo['name']) || empty($creatorInfo['email']) || empty($creatorInfo['phone']) ||
            empty($creatorInfo['company']))
        {
            return [
                'response' => '402',
                'message' => 'Missing creator details'
            ];
        }

        $input = [];
        $input['distributorKey'] = $distributorKey;
        $input['project'] = $projectInfo;
        $input['creatorInfo'] = $creatorInfo;

        $response = $this->getJsonPost($input, "/v2/Project/CreateDemo", 0, "", "POST", false, true);

        return $response;

    }


}