<?php

namespace EntegyPlugin\ApiV2;

use Src\Config;
use Firebase\JWT\JWT;

/**
 * Used to connect with the Entegy API.
 */

class ApiV2
{
    protected $apiSuffix;
    protected $secret;
    protected $projectId;
    protected $apiKey;
    protected $isProduction;
    protected $apiRegion;
    protected $dataFolder;

    /**
     * EntegyApi constructor. For use with EntegyPlugin.
     */
    public function __construct($config = [])
    {
        if(!$config)
        {
            $this->projectId = Config::PROJECT_ID;
            $this->apiKey = Config::API_KEY;
            $this->secret = Config::API_SECRET;
            $this->isProduction = Config::PRODUCTION_API;
            $this->apiRegion = Config::API_REGION;
            $this->dataFolder = Config::DATA_FOLDER;
        }
        else
        {
            $this->projectId = $config["projectId"];
            $this->apiKey = $config["apiKey"];
            $this->secret = $config["secret"];
            $this->isProduction = $config["isProductionApi"];
            $this->apiRegion = $config["apiRegion"];
            $this->dataFolder = $config["dataFolder"];
        }

        $this->apiSuffix = $this->getApiSuffix();
    }

    private function getApiSuffix()
    {
        if($this->isProduction === false || $this->isProduction === "false") return "https://api.entegy.rocks";

        switch($this->apiRegion)
        {
            case "eu":
                return "https://api-eu.entegy.com.au";
            case "us":
                return "https://api-us.entegy.com.au";
            case "au":
            default:
                return "https://api.entegy.com.au";
        }
    }

    /**
     * Execute the request. Optional caching functionality.
     * @param $jwtInput
     * @param $uri
     * @param int $cacheTime
     * @param string $cacheFileName
     * @param string $postType
     * @param bool $pagedRequest
     * @return array The server response. If 200, contains the content data.
     */
    protected function getJsonPost($jwtInput, $uri, $cacheTime = 0, $cacheFileName = '', $postType = "POST", $pagedRequest = false, $isDistributorRequest = false)
    {
        if (empty($cacheFileName))
            if (!empty($jwtInput)) $cacheFileName = $this->dataFolder . md5($uri . serialize($jwtInput)) . ".cac";
            else $cacheFileName = $this->dataFolder . md5($uri) . ".cac";
        else $cacheFileName = $this->dataFolder . $cacheFileName . ".cac";

        if ($cacheTime > 0 && file_exists($cacheFileName) && (time() - $cacheTime < filemtime($cacheFileName)) && !$pagedRequest)
            return unserialize(file_get_contents($cacheFileName));

        $baseInput = new ApiInput($this->projectId, $this->apiKey, $this->generateNounce());

        $inp = array_merge($baseInput->get(), $jwtInput);

        if ($isDistributorRequest) {
            unset($inp['projectId']);
            unset($inp['apiKey']);
        }

        $body = JWT::encode($inp, $this->secret, 'HS256');

        $fullUrl = $this->apiSuffix . $uri;

        $ch = curl_init($fullUrl);

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $postType);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/jwt'
        ]);

        $result = curl_exec($ch);

        // Separate Headers and Body
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $responseHeaders = explode("\r\n", substr($result, 0, $headerSize));
        $responseHeaders = array_filter($responseHeaders);
        $responseBody = substr($result, $headerSize);
        $responseHeadersFinal = [];
        foreach ($responseHeaders as $headerObj)
        {
            $header = explode(": ", $headerObj);
            if (isset($header[1]))
                $responseHeadersFinal[$header[0]] = $header[1];
        }

        curl_close($ch);

        $response = json_decode($responseBody, true);
        if (isset($responseHeadersFinal ['X-RateLimit-Limit']))
            $response ['X-RateLimit-Limit'] = $responseHeadersFinal ['X-RateLimit-Limit'];
        if (isset($responseHeadersFinal ['X-RateLimit-Remaining']))
            $response ['X-RateLimit-Remaining'] = $responseHeadersFinal ['X-RateLimit-Remaining'];
        if (isset($responseHeadersFinal ['X-RateLimit-Reset']))
            $response ['X-RateLimit-Reset'] = $responseHeadersFinal ['X-RateLimit-Reset'];

        if ($cacheTime > 0 && !$pagedRequest) {
            if(file_exists($cacheFileName)) {
                unlink($cacheFileName);
            }
            
            file_put_contents($cacheFileName, serialize($response));
        }

        return $response;
    }

    public function getPagedRequest($jwtInput, $uri, $key, $pagination = ["start" => 0, "limit" => 1000, "count" => 0], $cacheTime = 0, $cacheFileName = '')
    {
        if (empty($cacheFileName)) $cacheFileName = $this->dataFolder . md5($uri . serialize($jwtInput)) . ".cac";
        else $cacheFileName = $this->dataFolder . $cacheFileName . ".cac";

        if ($cacheTime > 0 && file_exists($cacheFileName) && (time() - $cacheTime < filemtime($cacheFileName)))
            return unserialize(file_get_contents($cacheFileName));

        $jwtInput ['pagination'] = $pagination;
        $response = [];
        do {
            $currResponse = $this->getJsonPost($jwtInput, $uri, 0, '', 'POST', true);

            if ($currResponse['response'] !== 200)
            {
                return array('response' => $currResponse['response'], 'message' => $currResponse['message']);
            }

            $response = array_merge($currResponse[$key], $response);

            $jwtInput ['pagination']['start'] += $currResponse['pagination']['limit'];
        }
        while(count($response) < $currResponse['pagination']['count'] && $currResponse['response'] == 200);

        $returnResponse[$key] = $response;
        $returnResponse['message'] = "success";
        $returnResponse['response'] = 200;

        if ($cacheTime > 0)
            if(file_exists($cacheFileName)) {
                unlink($cacheFileName);
            }

            file_put_contents($cacheFileName, serialize($returnResponse));

        return $returnResponse;
    }

    protected function generateRandomString($length = 10)
    {
        return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    protected function generateNounce()
    {
        $date = new \DateTime('now', new \DateTimeZone("UTC"));
        $date = $date->format("Y-m-d h:i");
        $nounce = $date . "|" . self::generateRandomString(20);
        return $nounce;
    }

}

